module.exports = {
  '/soking/': [
    '',
    'planlegg-soket-ditt',
    'soketeknikker',
    'systematisk-soking',
  ],
  '/studieteknikk/': [
    '',
    'lesemater',
    'lesing-og-skriving',
    'studiegrupper',
    'tidsstyring'
  ],
  '/skriving/': [
    '',
    'kom-i-gang-med-a-skrive',
    'struktur',
    'argumentere-redegjore-drofte',
    'oppbygning-av-en-oppgave',
    'imrad-modellen',
    'akademisk-sprak-og-stil',
    'formelle-krav-til-oppsett'
  ],
  '/kjeldebruk/': [
    '',
    'kjeldevurdering',
    'ulike-kjelder',
  ],
  '/frasebank/': [
    '',
    'nynorsk'
  ],
  '/referansestiler/': [
    '',
    'apa-7th',
    'chicago-forfatter-aar',
    'chicago-fotnoter',
    'harvard',
    'mla',
    'vancouver'
  ],
  '/kunstig-intelligens': [
    '',
  ],
  '/': [
    '/om/',
    '/om/kontaktinformasjon',
    '/om/sok-og-skriv-i-undervisning'
  ],
  '/ofte-stilte-sporsmal': [
    '',
  ]
}
