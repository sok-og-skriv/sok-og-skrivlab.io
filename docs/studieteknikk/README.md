---
title: "Studieteknikk"
date: "2012-03-15"
prev: "../soking/systematisk-soking"
tags:
  - lesing
  - studieteknikk
  - mestring
  - studiemestring

---

# ![](/images/illustrasjoner_lesing_500x450.png) Studieteknikk

Å være bevisst egen studieteknikk er til stor hjelp for deg som skal studere. Her kan du lese om planlegging, lesestrategier og skrivestrategier og få tips om hvordan du kan lese og forstå akademiske tekster, ta gode notater, styre tiden og samarbeide i studiegrupper.

::: tip Studiemestring 
Studieteknikk handler også om livskvalitet, mestring, kognitiv trening og gode rutiner. Nettressursen [Eksamenskoden](https://eksamenskoden.no) dekker mye av dette. Ta gjerne en tur innom! 
::: 

## Verktøy for god studieteknikk

Med Zotero og tilsvarende verktøy kan du samle både tekster du leser og notater du skriver på et sted. Det blir også lett å sitere kildene dine når du kommer til oppgaveskrivingen. Hvis du ønsker å samarbeide om å samle kilder og dele notater, kan du enkelt gjøre det i Zotero. 

Her kan du se hvordan Zotero kan gjøre din studiehverdag lettere: 



<Video id="SSckWJ7GuiM" title="Zotero gjør din studiehverdag lettere" />

## Lesing og skriving henger sammen

I det akademiske håndverket er lesing og skriving tett forbundet. For å kunne skrive en god oppgave, må du lese effektivt og målrettet. Når du trener deg opp som leser, vil du også kunne lese dine egne utkast og ideer på en oppmerksom måte. 

Hør Birger Solheims råd om hvordan du som student kan bli en bedre leser: 



<Video id="JchpFI50UDk" title="Birger Solheim om lesing" />

