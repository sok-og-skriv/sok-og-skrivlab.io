---
home: true
# heroImage: /home.png
# actionText:  Let's begin →
# actionLink: /en/searching/
footer: Search & Write - Creative Commons Attribution-NonCommercial-ShareAlike 4.0
---

<div class="cards">

  <div class="card">
    <div class="image">
      <a href="/en/searching/"><img src="/images/illustrasjoner_sok_500x450.png" alt="Searching"></a>
    </div>
    <div class="content">
      <h2><a href="/en/searching/">Searching</a></h2>
      <p>Searching for information can help you get started with your thesis and define the theme and scope of your project.</p>
    </div>
  </div>

  <div class="card">
    <div class="image">
      <a href="/en/study-skills/"><img src="/images/illustrasjoner_lesing_500x450.png" alt="Study skills"></a>
    </div>
    <div class="content">
      <h2><a href="/en/study-skills/">Study skills</a></h2>
      <p>Reading and writing are closely related activities in academic work. In order to write a good thesis, you need to be conscious of how and what you read.</p>
    </div>
  </div>

  <div class="card">
    <div class="image">
      <a href="/en/writing/"><img src="/images/illustrasjoner_skriving_500x450.png" alt="Writing"></a>
    </div>
    <div class="content">
      <h2><a href="/en/writing/">Writing</a></h2>
      <p>In academic writing there are certain formal requirements regarding form and contents, language and style.</p>
    </div>
  </div>

  <div class="card">
    <div class="image">
      <a href="/en/sources-and-referencing/"><img src="/images/illustrasjoner_kildehenvisning_500x450.png" alt="Sources"></a>
    </div>
    <div class="content">
      <h2><a href="/en/sources-and-referencing/">Sources</a></h2>
      <p>All research is based on sources. There are various criteria for assessing their relevance and quality.</p>
    </div>
  </div>
</div>

<div class="flex-cards one-column">
  <div class="card">
    <div class="image">
      <a href="/en/artificial-intelligence/">
        <img src="/images/illustrasjoner_KI_500x450.png" alt="">
      </a>
    </div>
    <div class="content">
      <h2>
        <a href="/en/artificial-intelligence/">
          AI in education
        </a>
      </h2>
      <p>Updated about the use of artificial intelligence in searching, writing, sources, and study skills</p>
    </div>
  </div>
</div>

<section style="margin-bottom:3em;">
  <div class="flex">
    <h2 style="border-bottom:none;">Current videos</h2>
    <small><a href="https://www.youtube.com/@skogskriv">...more</a></small>
  </div>
  <div style="aspect-ratio: 9/6 auto;">
  <iframe width="100%" height="100%" src="https://www.youtube-nocookie.com/embed/videoseries?si=hQvWJTMVcrYxUrCs&amp;list=PLrn0Z-Bf8orpOH2wkOz3v9-lRiHOKZU5A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
  </div>
</section>

## Partners

<div class="partners">
  <div class="partner">
    <a href="https://www.hvl.no" class="no-external-link-icon">
      <picture>
        <source media="(max-width: 719px)" srcset="/partners/hvl-icon.jpg">
        <img src="/partners/hvl-logo-en.jpg" alt="Western Norway University of Applied Sciences"/>
      </picture>
    </a>
    <div class="title">
      Western Norway University of Applied Sciences
    </div>
  </div>
  <div class="partner">
    <a href="https://www.uib.no" class="no-external-link-icon">
      <picture>
        <source media="(max-width: 719px)" srcset="/partners/uib-icon.png">
        <img src="/partners/uib-logo-en.png" alt="University of Bergen"/>
      </picture>
    </a>
    <div class="title">
      University of Bergen
    </div>
  </div>
  <div class="partner">
    <a href="https://www.uio.no" class="no-external-link-icon">
      <picture>
        <source media="(max-width: 719px)" type="image/svg+xml" srcset="/partners/uio-icon-en.svg">
        <img src="/partners/uio-logo-en.svg" alt="University of Oslo"/>
      </picture>
    </a>
    <div class="title">
      University of Oslo
    </div>
  </div>
</div>

---

<div class="container two-column footer-links">
  <div class="align-right">
    <div><a href="/om/">Om Søk og skriv</a></div>
    <div><a href="/om/kontaktinformasjon.html">Kontaktinformasjon</a></div>
    <div><a href="/om/sok-og-skriv-i-undervisning.html">Søk og skriv i undervisning</a></div>
    <div><a href="https://uustatus.no/nb/erklaringer/publisert/c6d9a394-b5ac-48fd-9f1f-0154b2daacbc">Tilgjengelighetserklæring</a></div>
  </div>
  <div class="align-left">
    <div><a href="/en/about/">About Search and write</a></div>
    <div><a href="/en/about/contact-information.html">Contact</a></div>
  </div>
</div>
