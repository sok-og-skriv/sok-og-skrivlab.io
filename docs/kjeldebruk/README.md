---
title: "Kjeldebruk"
date: "2012-03-15"
lang: "nn"
prev: "../skriving/formelle-krav-til-oppsett"
tags: 
  - Kjeldebruk
  - Kildebruk
  - Referansar
  - Plagiat
  - Plagiering
  - Opphavsrett
  - Sitat
  - Sekundærkilder
  - Sekundærkjelder
  - Sjølvsitering
  - Selvsitering
  - Sjølvplagiat
  - Selvplagiat

---
# Kjeldebruk
God forsking bygger vidare på eksisterande kunnskap; det er dette vi kallar kjeldebruk. Her lærer du om korleis du vel ut relevante kjelder, og korleis du brukar kjeldene i ditt akademiske arbeid.  

I denne videoen fortel professor ved HVL, Ole Bjørn Rekdal kva som kjenneteiknar god akademisk kjeldebruk.  

<Video id="GPR0phJIsuk" title="Ole Bjørn Rekdal - Kva kjennetegnar god akademisk kjeldebruk?" />


Store norske leksikon definerer ei kjelde som  "opphav til kunnskap eller informasjon". Ei kjelde i ein akademisk samanheng kan vere ein person eller informant, men viser hovudsakleg til tekstar og andre typar medium. Det kan vere oppslagsverk, fagbøker, avisartiklar, nettsider, bilde, videoar og så vidare. Eit sentralt kjenneteikn for denne typen kjelder er at dei har ein ansvarleg avsendar. Her kjem vi til å fokusere mest på det ein kallar teoretiske kjelder, slik som vitskaplege forskingsartiklar og fagbøker. Ein kan seie at desse kjeldene er [byggesteinane i akademisk arbeid](/kjeldebruk/ulike-kjelder.html#kjelder-til-fordjuping-og-grunngjeving). Difor er det forventa av deg som student at du lærer deg å bruke slike kjelder.  

Å skrive akademisk er å [delta i ein samtale](https://youtu.be/FJKg3G-JRpg?si=1ohV-pfJbPNca3Rv) der kjelder viser til andre kjelder. Kva kjelder som er best å bruke vil variere frå fag til fag og frå oppgåve til oppgåve. Bruk problemstillinga di til å orientere deg, og snakk gjerne med rettleiar, medstudentar og andre fagfolk for å finne ut kva kjelder som er gode innan ditt fagfelt.  

## Kvifor referere til andre sitt arbeid?
God kjeldebruk viser tydeleg kva du bygger videre på og skil dette frå dine eigne bidrag. Når du brukar kjeldetilvisingar på ein god måte undervegs i skrivinga di synleggjer du nettopp dette. Kjeldetilvisinga viser også lesaren vidare til referanselista, der ein finn fullstendig informasjon om opphavsperson, årstal og utgivar. Dette gjer det mogeleg for lesaren å finne tilbake til kjelda. Nøyaktig dokumentasjon av kjelder gjer lesaren i stand til raskt å

- finne tilbake til kjeldene 
- kontrollere og etterprøve opplysningar 
- setje seg inn i emnet 

Når du viser til og brukar kjelder i oppgåva di, brukar du andre sine verk. Det er viktig at du lærer deg korleis du kan bruke desse utan at det kjem i konflikt med gjeldande lov om [opphavsrett](/kjeldebruk/#opphavsrett).

Korrekt føring av referansar er òg avgjerande for å unngå [plagiat](/kjeldebruk/#unnga-plagiat-brot-pa-opphavsretten).

## Korleis refererer du?
Slik refererer du:
1. angi kjelda i teksten med ei tilvising 
2. oppgi fullstendig informasjon om kjelda i referanselista på slutten av teksten
 
Korleis kjeldetilvisingar og referanseliste skal sjå ut, vil avhenge av referansestilen. Finn ut kva for referansestil som blir brukt på ditt fag. Når du har valt ein referansestil, er det viktig at du følger denne konsekvent. 

På Søk & Skriv viser vi stilane [APA, 7. utgåve](/referansestiler/apa-7th.html), [Chicago med fotnoter](/referansestiler/chicago-fotnoter.html), [Chicago forfatter-år](/referansestiler/chicago-forfatter-aar.html), [Harvard](/referansestiler/harvard.html), [MLA](/referansestiler/mla.html) og [Vancouver](/referansestiler/vancouver.html). 

APA 7 er den mest vanlege referansestilen og derfor er dei fleste eksempla i Søk & Skriv henta frå denne stilen. 

## Kva skal refererast?
Når du hentar noko frå ei kjelde skal du alltid vise til denne kjelda både i teksten din og i referanselista. Dette gjeld alt frå talmateriale, modellar, resultat og konklusjonar til andre sine argument og vurderingar. Typiske kjelder å hente dette frå kan vere bøker eller forskingsartiklar. Hugs at all bruk av figurar, tabellar, lyd og bilde er knytt til [opphavsrett](/kjeldebruk/#opphavsrett). Sjå til dømes [Norsk APA manual](https://cms.sikt.no/sites/default/files/inline-images/APA%207th%20norsk%20%20-%202024%20%2Cv1.10.pdf) for å lese meir om dette. 

**Allmenne sanningar** 

Allmenne sanningar treng ikkje referanse, som for eksempel: 

«Danmark er ein del av Europa», «vi har fire årstider», eller at 17. mai er Noreg sin nasjonaldag. 

Skriv du derimot om noko som ikkje er allment kjend, må du vise kvar du hentar opplysningane frå. Det kan vere vanskeleg å vurdere kor langt dokumentasjonskravet skal gå, og det vil variere frå fag til fag kva som er rekna som allmenne sanningar. Er du i tvil, spør rettleiar. Då unngår du å bli mistenkt for å plagiere. 

## Unngå plagiat / brot på opphavsretten 

::: tip Korleis la vere å plagiere? 
_Det viktigaste er å aldri klippe og lime utan å oppgi kjelde._

_Skriv sitat ordrett med hermeteikn, eller parafraser (skriv innhaldet med eigne ord)._

_Når du parafraserer, kan du legge vekk kjelda medan du skriv, så du ikkje blir opphengd i skrivemåten til forfattaren. Sjekk deretter at meiningsinnhaldet stemmer, og sett inn kjeldetilvising._

**Vips, så har du unngått å plagiere!**
::: 

Omfattande og/eller medvite brot på opphavsretten kan bli rekna som forsøk på fusk, og kan få alvorlege følgjer for deg som student. Ved forsøk på fusk kan konsekvensane bli annullering av eksamen, utestenging frå institusjonen og tap av retten til å gå opp til eksamen ved andre universitet og høgskular i Noreg i inntil 2 år. 

Vis alltid kvar du har henta informasjon eller formuleringar frå – då er du trygg. Dette gjeld òg for ditt eige arbeid: omfattande gjenbruk av ditt eige arbeid kan nemleg også bli rekna som forsøk på fusk. 

**Sjå denne videoen for ei enkel innføring i korleis du setter inn kilder og unngår plagiat**

<Video id="3IIoBZ0Tf_I" title="Hvordan sette inn kilder og unngå plagiat" />

### Sjølvsitering eller sjølvplagiat? 

Å gjenbruke eige arbeid kan vere aktuelt, men dei færraste oppgåver blir betre av å trekke inn ting du har gjort tidlegare. Dersom du ikkje kjem utanom, gjeld desse råda: 

Publiserte oppgåver viser du til på vanleg måte, med forfattar, årstal, tittel og utgivar. 

Upubliserte studentarbeid kan visast til som «Eige/eget arbeid (årstal). Tittel på oppgåva [Upublisert semesteroppgåve eller det som passar]. Namnet på utdanningsinstitusjonen». 

Desse eksempla følger [APA-stilen](/referansestiler/apa-7th.html#studentoppgaver-og-doktorgradsavhandlinger) 

Mange studiestader har tydelege retningslinjer for gjenbruk av eige arbeid/sjølvsitering. Sjekk alltid med rettleiar eller faglærar dersom du ønsker å bruke tidlegare arbeid. 

### Opphavsrett 

Opphavsrett er den retten skaparen av eit åndsverk har til verket.  Åndsverket kan vere eit litterært, vitskapeleg eller kunstnarisk verk, og skaparen blir kalla opphavar. Opphavsretten er regulert i lov om opphavsrett til åndsverk frå 2018, også kalla [åndsverklova](https://lovdata.no/dokument/NL/lov/2018-06-15-40). Hovudregelen i åndsverklova er at opphavaren har einerett til å framstille eksemplar av verket og å gjere det tilgjengeleg for andre. Denne økonomiske retten varer i 70 år etter at opphavar er gått bort, då «fell verket i det fri», som det heiter. 

I tillegg til den økonomiske råderetten opphavar har, kjem den ideelle – det vil seie retten til å bli namngitt og verna mot at åndsverket blir brukt på eit krenkande vis. Merk at den ideelle retten ikkje går ut på dato. Brot på gjeldande reglar om opphavsrett kan få uheldige følgjer. Plagiat er å framstille andre sine resultat, tankar, idear eller formuleringar som om dei var sine eigne. Dette reknast som intellektuelt tjuveri ifølge [åndsverkslova](https://lovdata.no/dokument/NL/lov/2018-06-15-40). 

## Sitat 

Av og til kan det vere aktuelt å hente informasjon eller tekst frå andre inn i eigen tekst. Dette må gjerast i samsvar med god skikk, ved å sitere og vise korrekt til kjeldene. Gi aldri inntrykk av at noko er ditt når det ikkje er det.

### Direkte sitat 

Direkte sitat er ei heilt ordrett attgiving. Eksempel kan vere definisjonar, særleg gode formuleringar eller utsegn til vidare drøfting. Sitat som er mindre enn 40 ord (eller tre linjer) skal skrivast direkte inn i teksten og uthevast med hermeteikn. 

::: eksempel Eksempel
«When you are writing a thesis, connecting your topic to an *academic* discourse is essential» 
(Nygaard, 2017, s. 111). 

Kjelde: Nygaard, L.P. (2017). *Writing your master´s thesis. From A to Zen*. Sage.
::: 

Merk at dersom den opprinnelige teksten inneheldt kursiv, skal du også bruke kursiv når du gjer att sitatet.

Sitat som har meir enn 40 ord skal (i APA) skrivast i eit eige avsnitt med innrykk. Når du skriv sitata på denne måten skal det ikkje brukast hermeteikn, og punktum kjem før parentes. Det er vanleg å introdusere sitat med innleiing og/eller ein kommentar.

### Ved endringar i sitatet
På same måte som med indirekte sitat, kan det nokre gonger vere aktuelt å gjere endringar på sitatet. Det kan til dømes vere hensiktsmessig å å legge til ord for å få lesaren til å forstå samanhengen. Om du har eit lengre sitat med ein del irrelevante passasjar kan det vere aktuelt å **utelate** delar av sitatet. Sitnemnde må markerast ved tre punktum inne i hakeparentes. 

::: eksempel Eksempel
Det opphavelege sitatet:

«If your topic has been discussed in the media, among policy makers or in practice, a recap of this non-academic discussion can be included in the background» (Nygaard, 2017, s. 111).

Sitatet med utelatte delar:

«If your topic has been discussed in the media [...] a recap of this non-academic discussion can be included in the background» (Nygaard, 2017, s. 111).
::: 

Dersom du **tilføyer** noko i eit sitat, markerer du det hakeparentes. 

::: eksempel Eksempel
Det opphavelege sitatet:

«But what will make your work academic, is the extent to which you are also able to link your discussion to an academic discourse» (Nygaard, 2017, s. 111).

Sitatet med tilføying:

«But what will make your [student] work academic, is the extent to which you are also able to link your discussion to an academic discourse» (Nygaard, 2017, s. 111).
::: 

### Indirekte sitat 

Indirekte sitat, også kalla parafrase, syner til dei tilfella der du gir att innhaldet med dine eigne ord. Indirekte sitat kan bidra til å skape flyt i teksten og kan synleggjere at du har ei god forståing for kjelda. Pass på at innhaldet blir gitt att korrekt og at meininga framleis er den same. 

::: eksempel Eksempel 
Det opphavelege innhaldet:

> The literature review identifies for the reader exactly which academic conversation you want to be a part of. The difference between background and the literature review is comparable to the difference between reporting the scores of a football game and providing expert commentary. (Nygaard, 2017, s. 111)

Indirekte sitat:

Eit viktig poeng med å skrive ein litteraturreview, også kalla litteraturgjennomgang, som ein del av masteroppgåva, er å synleggjere for lesaren kva for ein akademisk samtale di oppgåve skal vere eit bidrag til. For å illusterere forskjellen mellom bakgrunn og ein litteraturreview, brukar Nygaard eit eksempel frå fotballen: Bakgrunnen er som å rapportere antall mål i en fotballkamp, mens ein litterturreview er utgreiinga til ekspertkommentatoren (2017, s. 111).
:::

## Sekundærkjelder 

Hovudregelen er at du kun skal vise til verk du har lese. Om originalkjelda ikkje er tilgjengeleg, eller er på språk du ikkje forstår, kan du vise til andre si omtale av kjelda: 

::: eksempel Eksempel 
I teksten: 

«Det er ikke tilfeldig at det moderne menneske skriver ’med’ en maskin» (Martin Heidegger, sitert i Johansen, 2003, s. 80). 

I referanselista: 

Johansen, A. (2003). *Samtalens tynne tråd: skriveerfaringer*. Spartacus.
:::

Dersom du kjenner til publikasjonsåret for primærkjelda, skal dette også inkluderast i teksten. Du kan lese meir om bruk av sekundærkilder i [Ofte stilte spørsmål](/ofte-stilte-sporsmal/#sekundærkilde-sekundærreferanse-sitat-hentet-fra-en-annen-kilde).

## Bruk av illustrasjonar og statistikk 

Du har lov til å bruke foto du har teke sjølv. Hugs at i dei fleste tilfelle må avbilda personar gi samtykke. 

Hovudregelen er at du må ha samtykke frå opphavar for å bruke andre sine foto og kunstverk.  Men brukar du eit kunstverk eller eit bilde i ein vitskapleg, ikkje-kommersiell samanheng – slik ei studentoppgåve vil vere – har du lov til å bruke dei i oppgåva di. Merk at desse verka og bilda ikkje kan brukast som reine illustrasjonar, men må inngå som ein del av argumentasjonen i oppgåva. 

Bruk av bilde og andre illustrasjonar er regulert i [åndsverkslova](https://lovdata.no/dokument/NL/lov/2018-06-15-40). 
Dersom du er usikker finn du meir detaljert informasjon i [APA-manualen](https://cms.sikt.no/sites/default/files/inline-images/APA%207th%20norsk%20%20-%202024%20%2Cv1.10.pdf) 

Mange universitet og høgskular tilbyr databasar med bilde som er lisensiert for fri bruk. Sjekk med din institusjon. Du kan òg finne bilde til fri bruk via Creative Commons Search, eller søke i Google og definere lisens under verktøy-fana.

::: tip Tips 
_[BONO](https://bono.no/) er ein opphavsrettsorganisasjon for bildekunst. Sjekk om din institusjon har avtale med BONO eller sjå om du kan søke om enkeltlisens._ 
:::  
 
 **Tabellar og grafar** 
 Dersom du skal bruke ein tabell eller ein graf frå eit publisert verk, er det ikkje tillate å kopiere han inn i ditt eige arbeid utan samtykke frå opphavar. Skriv heller med dine eigne ord kva tabellen eller grafen seier og vis til han, eller lag din eigen. Det enklaste kan vere å spørje opphavaren om lov.

## Notar og vedlegg 

I tillegg til sitat og referanseliste, kan ei oppgåve inneholde notar og vedlegg.

- Avgrens bruk av notar. 
- Notar skal brukast til tilleggsopplysningar som ikkje er ein naturleg del av teksten. 
- Du kan velje om du vil bruke fotnotar nedst på sida eller som sluttnotar bak i kapittelet/oppgåva. 
- Vel du å plassere notane som fotnotar er det vanleg å ha ein mindre skrifttype for å skilje notane frå hovudteksten. 
- Nøyaktig korleis notetilvisinga skal gjerast er avhenging av kva referansestil du brukar Det er for eksempel ikkje nødvendig med nummererte notar dersom du brukar ein nummerert referansestil som Vancouver. 
- Vedlegg er lister over tabellar og figurar som er med i oppgåva, spørjeskjema, observasjonsskjema, intervjuguide og liknande. Vedlegg skal nummererast og plasserast etter referanselista.

## Hjelpemiddel 

For større oppgåver kan du effektivisere arbeidet ved å bruke referanseverktøy som Zotero, EndNote, ReferenceManager eller Mendeley. Undersøk med studiestaden din kva referanseverktøy du har tilgang til. [Zotero](https://www.zotero.org/) gratis tilgjengeleg for alle, medan [Mendeley](https://www.mendeley.com/) og [EndNote](https://endnote.com/product-details/) finst i enklare gratisversjonar. 

Hugs at du alltid må dobbelsjekke at referansen blir rett! 

Her ser du korleis Zotero kan brukast:  

<Video id="SSckWJ7GuiM" title="Zotero gjør din studiehverdag lettere" />

::: tip Tips: 
_Det er fullt mogeleg å skrive ei oppgåve eller ein artikkel utan å bruke eit referansehandteringsverktøy. Referansar kan enkelt kopierast frå Oria, Google Scholar eller frå heimesida til tidsskriftet. Pass på å sjekke at dei er fullstendige og at eventuell kursiv følger med. Sjå etter sitatteikn:_

<List
  image="/images/scholar-cite.svg"
  alt="Scholar cite"
  text="Knapp for Google Scholar siteringsfunksjon."
/>

::: 




