---
title: Kunstig intelligens
date: "2024-10-183"
tags:
  - KI
  - AI
  - ChatGPT
  

---
# ![](/images/illustrasjoner_KI_500x450.png) Kunstig intelligens i utdanning


**KI-verktøy (f.eks ChatGPT og Microsoft Copilot) i Søk & skriv** 

Hovedfokuset for Søk & Skriv er skriving, søking, studieteknikk og kildebruk i høyere utdanning. Når det gjelder KI, er hovedfokuset for Søk & Skriv da hvordan store språkmodeller, som ChatGPT, kan brukes i slike sammenhenger.  

KI-verktøy som ChatGPT og Microsoft Copilot er laget for å generere tekst, ikke for å hente informasjon. Verktøyene er basert på språkmodeller som gjetter neste ord i en setning. De er trent opp på tekster som inneholder mye informasjon, men er ikke trent opp til å gjengi informasjonen korrekt. Verktøyene er derfor bedre egnet for formål der språklig flyt og sammenheng betyr mer enn at innholdet er pålitelig.  

Verktøyene er i en svært rask utvikling slik at det er vanskelig å utarbeide retningslinjer og lovverk i samme tempo. Søk & Skriv-redaksjonen gir generelle anbefalinger til trygg og formålstjenlig bruk, men sjekk retningslinjer på din institusjon og i ditt fagmiljø. 



 <Figure
  src="/images/ki-etikk komprimert.png" 
  alt="[vær kritisk og dobbelsjekk faktaopplysninger, ta hansyn til at ki-verktøy er ressurskrevende, ki er et hjelpemiddel, skriv, les og tenk selv]"
  caption=""
  type=""
/>
 


**Juridiske og etiske retningslinjer**

Når det gjelder bruk av KI, må du forholde deg både til juridiske og etiske retningslinjer, og du må vurdere hvor formålstjenlig, trygg og pålitelig bruken av verktøyet er.

Husk at det finnes andre metoder og verktøy som kan tjene ditt formål bedre. Et søk i Oria vil for eksempel gi deg bedre tilgang til relevante og kvalitetssikrete kilder enn Microsoft Copilot.   
 
Retningslinjer kan variere mellom institusjoner og fagdisipliner, og du bør alltid sjekke med ditt fagmiljø.  

::: eksempel Vær obs!
Husk å følge juridiske retningslinjer nedfelt i [EU AI Act](https://digital-strategy.ec.europa.eu/en/policies/regulatory-framework-ai), [GDPR](https://gdpr.eu/) og [lov om opphavsrett](https://lovdata.no/dokument/NL/lov/2018-06-15-40).
::: 
Vær forsiktig med hva slags informasjon du legger inn i en KI-chat. Mange studenter har tilgang til egne chat-løsninger fra sin institusjon, slik som GPT UiO og Sikt KI-chat. Når du bruker disse verktøyene, kan du dele såkalte grønne og gule data. Du skal alltid unngå å dele fortrolig eller strengt fortrolig informasjon. Under ser du en forklaring på hva som menes med ulike typer data:

<Figure
  src="/images/deldata (1).png" 
  alt="[ikke del sensitiv informasjon]"
  caption="Klikk på bilde for å lese"
  type=""
/>


Du må aldri dele opphavsbeskyttet materiale med åpne verktøy som ChatGPT. Innholdet fra materialet du legger inn kan potensielt bygges inn i verktøyet og dermed bli tilgengelig for andre som bruker det. Dette regnes som brudd på opphavsretten. Du kan lese mer om [bruk av KI-verktøy og opphavsrett her](https://www.uio.no/tjenester/ki/juridiskeforinger.html)


**Hjelp til vurdering av KI-verktøy**

Kunstig intelligens har gitt nye dimensjoner til informasjonsflyten. Samtidig skaper denne type verktøy økende behov for kritisk vurdering og navigering. Sjekklisten under kan hjelpe deg å analysere ulike KI-verktøy, og gi deg en pekepinn på om det kan være fornuftig å bruke til formålet du trenger det til, eller ikke.

::: tip Sjekkliste: Vurdering av KI-verktøy

Før du tar i bruk et KI-verktøy, kan det være lurt å undersøke om verktøyet er riktig for deg ved å stille noen spørsmål: 

1. **Løser verktøyet dine behov?**
Hvis du undersøker verktøyets bakgrunn og bruksområder, og etterspør andres erfaringer, så kan du danne deg en mening om nettopp dette verktøyet er det du bør lære deg å ta i bruk.  

2. **Hvem har laget verktøyet?**
Hvis du finner ut hvem som står bak verktøyet, hvor lenge de har jobbet med det, og hvilken bakgrunn de har, kan du danne deg en mening om blant annet kvalitet, målgruppe, erfaring og verktøyets hensikt. 

3. **Hva skjer med dataene dine?**
Når du bruker KI-verktøy, så leverer du som oftest fra deg tekst, bilder eller annen data. Studiestedet ditt kan ha strenge regler og retningslinjer for hva du har lov og ikke lov til å dele, og du må ta hensyn til opphavsretten på data du deler. Blir dataene dine brukt til opplæring av KI-verktøy? Profiterer noen på dataene dine? Hvis du ikke vet hva som skjer med dataene dine, bør du heller ikke bruke verktøyet.
:::


::: details Disse spørsmålene er basert på en grundigere sjekkliste du også kan bruke:
| **Utfyllende sjekkliste**                                                                 |
| ------------------------------------------------------------------------------ |
| **Hva er verktøyets formål?** Kan det gjøre arbeidet mer effektivt? Kan det gi arbeidet høyere kvalitet?                                                                 |
| **Hvem er målgruppen?** Er verktøyet brukervennlig? Trengs det omfattende opplæring? Hvilke fagområder støtter verktøyet?                                             |
| **Hva er verktøyet bra til?** Sterke og svake sider ut ifra formålet? Nyttig for flere trinn i søkeprosessen? Mulig å eksportere til andre verktøy?                                                                         |
| **Hvem er verktøyets utvikler/leverandør?**                                    |
| **Hvilke kilder bruker verktøyet?** Hvilke typer data hentes ut? Hvilket format må/kan dataene ha?                                                                      |
| **Behandler verktøyet sensitive data?**                                        |
| **Etiske betenkeligheter ved verktøyet?**                                      |
| **Må man opprette konto?**                                                     |
| **Kjører verktøyet i skyen, eller lokalt på maskinen?**                        |
| **Har verktøyet åpen kildekode?**                                              |
| **Sannsynlig med fremtidig vedlikehold av verktøyet?**                         |
:::

 
::: oppgave Vurder selv
KI verktøy er ressurskrevende. Ta også hensyn til det når du vurderer nytteverdien av verktøyet.
:::


## Søking

Det finnes en rekke ulike verktøy basert på kunstig intelligens som er utviklet (og stadig utvikles!) for å finne relevante kilder, f.eks. [Keenious](https://keenious.com/), [Elicit](https://elicit.com/), [ResearchRabbit](https://www.researchrabbit.ai/) m.fl. Disse verktøyene kan være nyttige hvis du kun skal finne noen relevante artikler knyttet til en problemstilling, i den innledende fasen av et søk , eller når du skal gjøre søk som *supplerer* (altså kommer i tillegg til) et utført litteratursøk. Disse verktøyene alene egner seg ikke til [systematiske litteratursøk](/soking/systematisk-soking.html), da søkene ikke tilfredsstiller kravene til [etterprøvbarhet](/soking/systematisk-soking.html#hvordan-søke-systematisk). 
 
 [KI-verktøy som ChatGPT og Microsoft Copilot](/kjeldebruk/ulike-kjelder.html#tekst-fra-ki-verktøy-chatgpt-copilot) kan være nyttige når du skal komme i gang med en søkestrategi, da disse kan komme med enkle forslag til hvordan du kan sette opp et søk for en problemstilling. Vær klar over at forslagene fra slike KI-verktøy ofte kan inneholde feil. Det er viktig å være kritisk og nøye sjekke for blant annet feil søkeord, stavefeil eller feil bruk av AND/OR. For å kunne kvalitetssikre søkestrategiene, må du ha god kunnskap om [akademiske litteratursøk](/soking/) og fagfeltet du jobber innen. 

Hvis du vil teste ut KI-verktøy enten for å hjelpe deg med å finne [ulike synonymer for et faglig begrep](/soking/planlegg-soket-ditt.html#finn-gode-søkeord), komme i gang med [søkestrategien](/soking/soketeknikker.html), eller komme med tips til relevante [vitenskapelige artikler](/kjeldebruk/ulike-kjelder.html#vitskaplege-artiklar-tidsskrift-og-antologiar), er det viktig at du gir verktøyet tydelig formulerte instrukser (også kjent som [prompting](https://www.uio.no/tjenester/ki/student/), se under "Utforske og bruke") hvor du presiserer kriteriene for det du er på jakt etter.    


 :::tip Tips  
Spør gjerne en fagbibliotekar på studiestedet ditt om gode råd og hjelp til å finne og bruke kvalitetssikrede søkekilder når du skal finne vitenskapelig litteratur til oppgaven din. 
::: 


## Skriving

I akademisk sammenheng er det avgjørende at man forholder seg til kilder som har en ansvarlig avsender. Dette forplikter deg også til å stå ansvarlig som avsender for de tekstene du selv skriver som student i en faglig sammenheng. 

Å ta stilling til vurderingene og påstandene i en tekst, er særlig viktig når du bruker KI i skriveprosessen. Dette er fordi KI-generert tekst, i motsetning til en innføringsbok eller fagartikkel, ikke har en ansvarlig avsender. Du må derfor gjøre hele jobben selv. Hvis du for eksempel ber et trygt KI-verktøy om å skrive et sammendrag av argumentet i en primærkilde, er det derfor avgjørende at du selv også kontrollerer om dette faktisk stemmer med denne primærkilden.   

For å vurdere om du bruker KI-verktøy på en måte som gjør at du bevarer eierskapet ditt til teksten, kan du prøve å svare «ja» eller «nei» på disse spørsmålene:  


 <Figure
  src="/images/Flytskjema jaunar 25.png" 
  alt="[1) Er bruken av KI-verktøyet avgjørende for innholdet i teksten?
NEI: trygt å bruke (for eksempel til enkel rettskriving)
JA: til 2.
2) Er du i stand til å vurdere om innholdet i den (delvis) genererte teksten er til å stole på?
NEI: IKKE trygt å bruke
JA: til 3.
3) Kan du ta fullt ansvar for innholdet både juridisk og moralsk?
NEI: IKKE trygt å bruke
JA: mulig å bruke (for eksempel til språkvask, idemyldring og strukturering av teksten)]"
  caption=""
  type=""
/>
 



  


Generativ KI som ChatGPT er god på mønstergjenkjenning i store datamengder. Det betyr at den er rimelig pålitelig som verktøy for å gjenkjenne regelmessigheter og avvik på disse i tekst, som rettskriving, setningsstruktur og vanlige disposisjoner. Dette gjelder særlig på språk som er godt representert i treningsmaterialet.

::: oppgave Vurder selv         
 Tekstgenererende KI-verktøy gir ikke pålitelige svar i faktaspørsmål, eller i komplekse eller skjønnsmessige spørsmål som krever en rik kontekstforståelse. Disse spørsmålene bør du fortsatt finne svar på i faglitteraturen, eller diskutere med veileder og medstudenter.
:::

KI-verktøy kan være nyttige for å støtte skriveprosesser og utvikle skrivekompetanse, spesielt dersom du skriver på ditt andre- eller tredjespråk og/eller har lese- og skrivevansker. Verktøyet blir nyttigere jo mer du lærer deg å snakke (chatte) med det. Poenget er altså ikke å «få svar» eller ferdig tekst, men å bruke språkmodellen til å forbedre _egen tekst_, i et språk som gir god mening for en leser.

::: details Her er et eksempel på hvordan du kan jobbe:  

- Be chatboten om å skrive om mindre deler av teksten din til et lettere/mer direkte/klarere språk.  

- Be den så om å forklare hva den gjorde (for eksempel, på hvilken måte er den nye teksten lettere/klarere osv.). Forklaringen vil være en nyttig tilbakemelding til deg som skriver.  

- I et tredje trinn kan du be den om å begrunne sine valg. Da vil chatboten forklare hvorfor det er lettere å forstå setningsstrukturen, ordvalget etc. som den har valgt.  

- Til syvende og sist må du selv vurdere om det ‘nye’ språket er noe du vil bruke, og om du kjenner igjen din egen stemme i det, eller om du heller vil beholde den opprinnelige teksten.  

Dersom du bruker språkmodeller på denne måten, så ligner det på bruk av eksisterende verktøy som ordliste (for eksempel i Word), Google Translate og nettsøk etter fraser.  Det er vanligvis likevel god skikk å redegjøre for hvilke KI-verktøy du har brukt og hvordan du har brukt dem. Dette hjelper leseren av teksten din å være mer oppmerksom på skjevheter og feil som kun tekstgenerende KI-verktøy gjør.  

  
:::
  
::: eksempel Vær obs!    
Om du bruker ChatGPT eller tilsvarende språkmodeller til å produsere ferdig tekst som skal leveres inn til vurdering, eksamen o.l., vil dette kunne regnes som fusk. Det er viktig at du undersøker retningslinjene for ditt fag og din utdanningsinstitusjon.
:::

## Kildebruk

**Tekst fra KI-verktøy (ChatGPT og Microsoft Copilot)**   

[Tekst fra KI-verktøy regnes ikke som en kilde](/kjeldebruk/ulike-kjelder.html#tekst-fra-ki-verkt%C3%B8y-chatgpt-copilot) i akademisk sammenheng. Et sentralt krav til kilder i denne sammenhengen, er at de skal ha en ansvarlig avsender, som for eksempel en eller flere forfattere eller en organisasjon. Dette er ikke tilfellet for tekster generert av KI-verktøy. Tekstgeneratorer som ChatGPT, Microsoft Copilot og lignende kan dermed heller ikke gis ansvar som medforfatter. KI-verktøy regnes som programvare, og det er derfor kun i særskilte tilfeller at referanse til tekst fra KI-verktøy skal inkluderes i litteraturlisten. Siden slike verktøy påvirker resultatet av skriveprosessen din, er det likevel god skikk å redegjøre for hvilke verktøy du har brukt og hvordan du har brukt dem. 

::: tip Tips 
En KI-generert tekst er det ikke mulig for andre å gjenskape, og dermed heller ikke mulig å etterprøve. I oppgaven din bør du derfor beskrive hvordan du har brukt KI-verktøyet. 
::: 

Når du redegjør for bruken, kan være aktuelt å gjengi det du skrev inn i chatten, inkludert tidspunkt, omfang og hvordan resultatet ble integrert i teksten din.

::: oppgave Vurder selv
Du bør alltid selv kontrollere og vurdere om tekst fra KI-verktøy som ChatGPT og Microsoft Copilot er brukbar til ditt formål. 
::: 

**Når skal du referere til KI-verktøy?** 

Det skal refereres til KI-verktøy dersom verktøyet du har brukt virker inn på resultater, analyser og funn i oppgaven din, og når du skaper innhold med et verktøy for å illustrere hvordan det fungerer. Referer på vanlig måte med henvisning i tekst og oppgi fullstendig referanse i litteraturlisten. I en del sammenhenger vil det også være aktuelt å legge ved svaret fra chatten som et vedlegg i oppgaven.  
  
**Hvordan refererer du til KI-verktøy?**

[Referansestilen APA 7th](/referansestiler/apa-7th.html#kunstig-intelligens-f-eks-store-sprakmodeller-som-chatgpt-microsoft-copilot-mfl) har laget en anbefaling for hvordan du kan henvise til KI-genererte tekster. 

::: eksempel Vær obs!
Vær oppmerksom på at tekster generert av kunstig intelligens, for eksempel i form av store språkmodeller, er forbundet med etiske problemstillinger, og at bruk i studiesammenheng kan komme inn under bestemmelser om fusk og plagiering. Undersøk hva som er tillatt ved ditt studium hvis du vurderer å benytte en slik tjeneste. 
::: 

 ## Studieteknikk

Det er viktig å huske at KI i beste fall kan være et nyttig verktøy eller hjelpemiddel for studiene, og ikke en erstatning for [å lese eller skrive selv](https://www.sokogskriv.no/studieteknikk/). I skjemaet under finner du noen nyttige tips til hvordan du kan bruke KI som et hjelpemiddel for å bli en bedre student.


:::: tip Tips   

| Ferdigheter | Øvelser                                                                                                                                                                                                                                                          |
| ----------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Skape       | Be KI om å sette opp en tidsplan for et lengre prosjekt, som en semesteroppgave.                                                                                                                                                                                 |
| Evaluere    | Gi KI rollen som kritiker eller konstruktiv veileder som skal gi deg tilbakemelding på en tekst du har skrevet, f.eks. en diskusjon eller analyse.                                                                                                                                     |
| Analysere   | Be KI om å gi deg scenarioer hvor du spiller rollen som en fagperson som skal sette diagnoser, løse et rettsspørsmål, o.l. Du kan også be KI argumentere mot deg, som en sparringspartner.                                                                       |
| Anvende     | Gi KI rollen som en medstudent som kan hjelpe deg med idemyldring og disposisjon til en oppgave du skal skrive.                                                                                                                                                  |
| Forstå      | Gi KI rollen som en interessert lytter. Forklar KI hvordan du har forstått en forelesning eller tekst.* Hvis du merker at noen begrep er vanskelig å forstå eller sammenhenger er uklare for deg, så kan du be KI om å gi definisjoner eller komme med eksempler. |
| Huske       | Be KI om å gi deg kontrollspørsmål for å sjekke at du kan gjengi innholdet i det du har lest.                                                                                                                                                                    |

:::: 









Tabellen er inspirert av ressurs fra [Oregon State University](https://ecampus.oregonstate.edu/faculty/artificial-intelligence-tools/meaningful-learning/)


::: eksempel Vær obs!
Ikke del opphavsbeskyttet informasjon med [åpne KI-verktøy](https://www.sokogskriv.no/kunstig-intelligens/).
:::

 ## Eksterne ressurser

 + [UiO KI](https://www.uio.no/tjenester/ki/)

 + [UiB KI](https://www.uib.no/student/164404/verktøy-basert-på-kunstig-intelligens-i-utdanning)

 + [HVL KI](https://www.hvl.no/kunstig-intelligens/)

 + [PhD on Track: AI](https://www.phdontrack.net/open-science/AI.html)

 + [EU AI ACT](https://digital-strategy.ec.europa.eu/en/policies/regulatory-framework-ai)

 + [Digdir KI-ressurser](https://www.digdir.no/kunstig-intelligens/ki-ressurser/4145)

  
 

 
