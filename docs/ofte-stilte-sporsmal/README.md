---
title: Ofte stilte spørsmål
date: "2022-06-03"
tags:
  - spørsmål
  - FAQ
  - "ingen kategorier passer"
  - "flere kilder i samme parentes"
  - sekundærkilde
  - sekundærreferanse
  - primærkilde
  - "sitat hentet fra annen kilde"
  - sidetall
  - DOI
  - "Fagfellevurdert artikkel"
  - "Peer review"
  - ChatGPT
  - "Kunstig intelligens"
  - KI
  - "Artificial intelligence"
  - AI
  - språkmodeller
  - "Large language models"
  - "Bing Chat"
  - Bard
  - nasjonale retningslinjer
  - nasjonal faglig retningslinje
  - kunnskapsbasert praksis
  - kunnskapsbasertpraksis
  - helsebiblioteket

---

# ![](/images/illustrasjoner_faq_500x450.png) Ofte stilte spørsmål 

Denne siden er under utarbeidelse. Flere spørsmål og svar kommer.

## Kildebruk og akademisk skriving


### Hva er DOI?

DOI står for digital object identifier, og er et unikt nummer som brukes for å identifisere elektroniske dokumenter. Når du skal referere til en elektronisk artikkel, bør du bruke DOI dersom dette finnes. DOI er mer stabilt enn en URL, og vil vise til riktig dokument selv om plasseringen på nett endrer seg. 

::: tip Tips
Som oftest finner du DOI-nummeret øverst eller nederst på framsiden av artikkelen, eller i trefflista i databasen. 

Eksempel på DOI-nummer: 10.1177/1090198109343895


Når du skriver en referanse som har DOI-nummer, må du legge til https://doi.org/  foran nummeret. 

:::


### Hva er en fagfellevurdert artikkel (peer review)?

::: oppgave Mer om dette:

[Du finner svar på spørsmålet her](https://www.sokogskriv.no/kjeldebruk/ulike-kjelder.html#fagfellevurdering-peer-review)    


:::



### Hvordan kan kunstig intelligens (f.eks ChatGPT) brukes? 

::: oppgave Mer om dette: 

[Du finner svar på spørsmålet her](https://www.sokogskriv.no/kunstig-intelligens/)


:::



## APA 7



### Flere kilder i samme parentes

Bruker du flere kilder i samme setning eller avsnitt, kan disse settes i samme parentes. Du skiller dem med semikolon.
Bruk sidetall dersom det er aktuelt. 

**Ulike forfattere**:
Kildene skal stå i alfabetisk rekkefølge.

::: warning Eksempel: 

(Bergersen, 2017; Tvedt, 2021)

(Hermansen, 2018, s. 45; Stenehjem, 2023, s. 88)

:::



**Samme forfatter**:
Kildene skal stå i kronologisk rekkefølge, eldste til nyeste. Sett komma mellom kildene.

::: warning Eksempel: 
(Johansen, 2005, 2018)

(Hatlevoll, 2011, s. 32, 2022, s. 12)
:::



###  Forfatter mangler

 
Hvis du ikke finner informasjon om forfatter, skriver du tittel der det vanligvis ville stått forfatter i henvisningen i teksten og i referansen. Dreier det seg om en bok, setter du tittelen i kursiv. Gjelder det en artikkel, eller en annen del av et større verk, bruker du anførselstegn. 

::: warning Eksempel:

... («Selfangst», 1998)  

Selfangst. (1998). I P. Henriksen (Red.), _Store norske leksikon_ (3. utg., Bd. 13, s. 132–133). Kunnskapsforlaget. 


:::

Ofte er det en institusjon eller organisasjon som er ansvarlig for teksten. I slike tilfeller bruker du ansvarlig oganisasjon/institusjon som forfatter. 

::: warning Eksempel:

… (Høgskulen på Vestlandet, 2020)  

… (Søk & Skriv, 2023) 

:::


### Hvordan referere til kunnskapsbasert praksis-sidene på Helsebiblioteket (Kunnskapsbasertpraksis.no)

::: oppgave Mer om dette:

 [Du finner svar på spørsmålet her](https://www.sokogskriv.no/referansestiler/apa-7th.html#nettside-fra-organisasjon)

Se oppsett under "flere eksempler".


:::
### Hvordan referere til nasjonale retningslinjer

::: oppgave Mer om dette:

 [Du finner svar på spørsmålet her](https://www.sokogskriv.no/referansestiler/apa-7th.html#standarder-veiledere-og-retningslinjer)

Se oppsett under "flere eksempler".

:::



### Ingen kategorier passer

::: oppgave Mer om dette:

 [Norsk APA-manual har flere eksempler enn Søk & Skriv](https://www.unit.no/tjenester/norsk-apa-referansestil)

:::

Finner du ingenting som passer her heller, kan du lage din egen variant. Da er det viktig at du er konsekvent og at det i teksten din og referanselista kommer fram opplysninger som kan identifisere kilden.


Du trenger fire typer opplysninger for å lage en referanse:
+ **Hvem**: Forfatter, institusjon/organisasjon, departement osv. 
+ **Når**: Utgivelsestidspunkt. Vanligvis årstall, men for noen nettkilder må du også ha dato for siste oppdatering.
+ **Tittel**: Fins det ikke en tittel, kan du lage en beskrivelse i skarpe klammer.
+ **Hvor**: Forlag, navn på nettsted, departement, el.lign. Hvis du har DOI eller URL tar du dette med. 



 
::: warning Til sammen blir dette:

Forfatter. (År). Tittel [evt. beskrivelse i skarpe klammer]. Utgiver. URL eller DOI
:::

### Kapittel i redigert bok

::: oppgave Mer om dette:

 [Du finner svar på spørsmålet her](https://www.sokogskriv.no/referansestiler/apa-7th.html#kapittel-i-redigert-bok)

:::

### Sekundærkilde/sekundærreferanse (sitat hentet fra en annen kilde)

Bruker du et sitat fra en annen kilde enn den du leser, skal du vise hvor denne kilden har hentet sitatet fra. Det kaller vi sekundærreferanse. Kilden der sitatet opprinnelig stod, kalles primærkilden. Det beste er å gå til primærkilden dersom dette er mulig. Er primærkilden vanskelig å få tak i, kan du vise til den sekundære kilden. Oppgi primærkilden og "henvist i", "sitert i", "referert i" eller liknende etterfulgt av kilden du har brukt. Bruk årstall dersom det er tilgjengelig.

::: warning Eksempel:



Du ønsker å skrive en tekst om Bandura sin teori om _egenvurdert mestringsforventning_. Bandura har presentert denne teorien i boka «Self-efficacy: the exercise of control». Denne boka er **primærkilden**. Du får ikke tak i boka, men du har lest om teorien i Gunn Imsen sin lærebok «Elevens verden». I denne boka viser Imsen til boka «Self-efficacy: the exercise of control» når hun beskriver teorien. Imsen sin bok blir **sekundærkilden** dersom du referer til denne i din tekst.      

**Du skriver følgende:**

>Bandura mener at mestringsforventninger (Self-efficacy) spiller en rolle når det gjelder hvilke aktiviteter vi vil delta i, og hvor mye energi vi vil investere i gjennomføringen. Har man liten tro på at man vil klare aktiviteten, blir >det lagt mindre innsats ned enn om man har stor tro på resultatet (Bandura, 1997, referert i Imsen, 2020. s. 362).



**Forklaring:**

Det må komme fram av referansen at det du refererer til, er noe forfatteren av boka har hentet fra en annen kilde. Imsen hadde referert til Bandura (1997), derfor må dette med i referansen.

**I referanselisten er det sekundærkilden som skal føres opp:**

>Imsen, G. (2020). Elevens verden. _Innføring i pedagogisk psykologi._ (5. utg). Universitetsforlaget.

**Forklaring:**

Det er denne boka du har lest. Du skal *aldri* føre opp noe på referanselisten som du ikke lar lest selv!

 
:::


  
  


### Sidetall

Sidetall er obligatorisk ved bruk av direkte sitat og anbefalt ved bruk av parafrasering/når du skriver med egne ord. 

Viser du til tekst over flere sider, oppgir du første og siste side.

::: warning Eksempel: 
(Dalland, 2020, s. 42-51)
:::

  


### Sidetall mangler

Noen kilder mangler sidetall; det kan være nettsider, elektroniske dokumenter eller enkelte artikler. 

Dersom du ønsker å bruke direkte sitat, bør du likevel vise til den aktuelle delen av teksten. 
Bruk for eksempel avsnittsnummer, kapittel- eller underkapitteloverskrift, paragrafnummer (lover), artikkel, tabell, figur eller lignende. Om ingen av disse er aktuelle, kan du telle avsnitt selv. Det viktigste er at leseren kan finne tilbake til det du siterer.


::: warning Eksempel:
... (Friluftsloven, 1957, § 10)

... (Norsk institutt for vannforskning, 2022, avsn. 4)
:::

### Utgave eller opplag?



Når en bok kommer i **ny utgave eller revidert opplag**, betyr det at innholdet er endra siden sist, og opplysninger om dette skal med i referanselista. Merk at det ikke skal med utgaveopplysninger om 1. utgave, og at du alltid skal vise til den utgaven du har lest.

::: warning Eksempel:
Dalland, O. (2020). _Metode og oppgaveskriving_ (7. utg.). Gyldendal.

Skoglund, E. (2018). _Anatomi og fysiologi: Kort og godt_ (Rev. oppl.). Fenris forlag.




:::

**Nytt opplag** betyr at boka er trykt opp i flere eksemplarer uten at innholdet er forandra. Opplysninger om opplag skal derfor ikke med i referanselista. 





###  År mangler

Det er ikke alle dokumenter som har årstall. 

På nettsider kan du lete etter årstall for siste oppdatering. I bøker, artikler og rapporter finner du som oftest utgivelsens årstall på tittelsiden. Dersom det ikke er mulig å finne, bruker du forkortelsen u.å. (uten år). 


::: warning Eksempel:

 Helsedirektoratet (u.å.)

 Johannessen (u.å.)
:::


















